create database bja-mysql;
use bja-mysql;

CREATE TABLE Instruments(
    id int not null auto_increment,
    inst_name varchar(50) NOT NULL,
    PRIMARY key (id)
);

CREATE TABLE Counterparties(
    id int AUTO_INCREMENT,
    count_name varchar(50) NOT NULL,
    PRIMARY key (id)
);

CREATE TABLE Deals_history (
    deal_id int NOT NULL auto_increment,
    inst_id int NOT NULL,
    count_id int NOT NULL,
    price float NOT NULL,
    type char,
    quantity float NOT NULL,
    PRIMARY KEY (deal_id),
    FOREIGN KEY (inst_id) REFERENCES Instruments(id),
    FOREIGN KEY (count_id) REFERENCES Counterparties(id)
);
CREATE TABLE login_details(
    id int not null auto_increment,
    email_address varchar(100) not null,
    password varchar(100) not null,
    primary key (id)
);

