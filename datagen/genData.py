from flask import Flask, Response
from flask_cors import CORS

from RandomDealData import RandomDealData

app = Flask(__name__)
CORS(app)
random_deal_data = RandomDealData()
instrument_list = random_deal_data.createInstrumentList()


@app.route('/')
def index():
    def eventStream():
        while True:
            data = random_deal_data.createRandomData(instrument_list)
            yield 'data:{}\n\n'.format(data)  # For SSE, have to return a string object
    return Response(eventStream(), status=200, mimetype='application/json')


def bootapp():
    app.run(threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
    bootapp()
