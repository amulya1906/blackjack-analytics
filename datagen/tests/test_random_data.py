import pytest
import json
from datetime import datetime as dt

from .. import RandomDealData

TIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")

random_deal_data = RandomDealData.RandomDealData()
instrument_list = random_deal_data.createInstrumentList()
random_data = random_deal_data.createRandomData(instrument_list)
data_json = json.loads(random_data)


def test_instrument_list_len():
    assert len(instrument_list) == len(instruments)


def test_random_data_names():
    assert data_json["instrumentName"] in instruments


def test_random_data_cpty():
    assert data_json["cpty"] in counterparties


def test_random_data_price():
    assert isinstance(data_json["price"], float)


def test_random_data_qty():
    assert isinstance(data_json["quantity"], int)


def test_random_data_time():
    dtimeobj = dt.strptime(data_json["time"], TIME_FORMAT)
    assert dtimeobj < dt.now()
