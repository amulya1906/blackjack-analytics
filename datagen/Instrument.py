import numpy


class Instrument:
    def __init__(self, name, variance, drift, starting_price):
        self.name = name
        self.__variance = variance
        self.__drift = drift
        self.__price = starting_price
        self.__startingPrice = starting_price

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0, 1) * self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__startingPrice * 0.4:
            self.__drift = (-0.7 * self.__drift)
        self.__price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.__price

    def getVariance(self):
        return self.__variance

    def getDrift(self):
        return self.__drift

    def getPrice(self):
        return self.__price

    def getStartingPrice(self):
        return self.__startingPrice