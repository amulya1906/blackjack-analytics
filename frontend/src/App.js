import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import Login from './components/Login';
import Signup from './components/Signup';
import Header from "./components/Header";
import Footer from "./components/Footer";
// import 'bootstrap/dist/css/bootstrap.css';


const LOGURL = `http://localhost:4000/login`;

function App() {
  return (
    <Router>
      <div className="container">
        <Header />
        <div className="container">
          {/* {!onlineStatus && !loading ? (
            <h3>The data server may be offline, changes will not be saved</h3>
          ) : null} */}
          <Switch>
            {/* <Route
              path="/login"
              exact
              // render={() => <AllTodos allTodos={todos} loading={loading} />}
              render={() => <Login />}
            /> */}
            <Route exact path="/" component={Login} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
            {/* {/* <Route
              path="/add"
              render={() => <AddTodo submitTodo={submitTodo} />
            />
            <Route
              path="/edit/:_id"
              render={props => (
                <AddTodo {...props} submitTodo={submitTodo} todos={todos} />
              )}
            /> */}
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
