from flask import Flask, Response, request
from flask_cors import CORS
import mysql.connector
import json
import requests

# Database details
DB_PORT = '3306'
DB_NAME = 'login_database'

app = Flask(__name__)
CORS(app)


# Server from which to receive trading data
# DATAGEN_IP = '192.168.99.100'  # Docker container
DATAGEN_IP = 'localhost'
DATAGEN_PORT = '5000'
DATA_URL = f'http://{DATAGEN_IP}:{DATAGEN_PORT}/'

# Store Trading data
instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
prices = {inst: 0.0 for inst in instruments}


def showPriceUpdate(line):
    try:
        line_str = "{" + line.decode("utf-8") + "}"
        return line_str + "\n\n"
        data = json.loads(line_str)["data"]
        return type(data).__name__
        # prices[instName] = data["price"]
        # return f'{data["instrumentName"]}: {prices[instName]}'
    except Exception as exception:
        return type(exception).__name__


# # Receive and store incoming data
@app.route('/readPrices')
def index():
    def dataStream():
        data = requests.get(DATA_URL, stream=True)
        for line in data.iter_lines(chunk_size=1):
            if line:
                yield showPriceUpdate(line)
    return Response(dataStream(), status=200, mimetype='text/event-stream')


@app.route("/teststr")
def teststr():
    return "This is a test"


def bootapp():
    app.run(port=9000, threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
    bootapp()
