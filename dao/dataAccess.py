from flask import Flask, Response, request
from flask_cors import CORS
import mysql.connector
import json
import requests

# Server from which to receive data
DATAGEN_IP = '192.168.99.100'  # Docker container
# DATAGEN_IP = '0.0.0.0'  # Localhost
DATAGEN_PORT = '5000'
DATA_URL = f'http://{DATAGEN_IP}:{DATAGEN_PORT}/'

# Database details
DB_PORT = '3306'
DB_NAME = 'login_database'

app = Flask(__name__)
CORS(app)

# , methods=['POST'])
@app.route('/login', methods=['POST'])
def login():
    try:
        if request.method == "POST":
            with open('data.txt', 'w') as outfile:
                json.dump(request.data, outfile)
            return request.data
    except Exception as exception:
        return type(exception).__name__


# config = {
#     'email': email,
#     'password': password,
#     'host': 'db',
#     'port': DB_PORT,
#     'database': DB_NAME
# }
# connection = mysql.connector.connect(**config)
# cursor = connection.cursor()
# cursor.execute(f'SELECT * FROM {DB_NAME} WHERE email == {email} AND password == {password}')
# results = [{email: password} for (email, password) in cursor]
# cursor.close()
# connection.close()
# return results


# @app.route('/')
# def index():
#     return json.dumps({'favorite_colors': favorite_colors()})


# # Receive and store incoming data
# @app.route('/')
# def index():
#     def dataStream():
#         data = requests.get(DATA_URL, stream=True)
#         for line in data.iter_lines(chunk_size=1):
#             if line:
#                 yield '[Reading] data:{}\n\n'.format(line)  # TODO - remove [Reading]
#     return Response(dataStream(), status=200, mimetype='application/json')



# # Receive and store incoming data
@app.route('/read')
def index():
    def dataStream():
        data = requests.get(DATA_URL, stream=True)
        for line in data.iter_lines(chunk_size=1):
            if line:
                yield '[Reading] {}\n\n'.format(line)  # TODO - remove [Reading]
    return Response(dataStream(), status=200, mimetype='text/event-stream')


@app.route("/teststr")
def teststr():
    return "This is a test"


def bootapp():
    app.run(port=7000, threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
    bootapp()
